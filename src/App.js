import { useEffect, useState } from 'react';
import Form from './components/Form';
import SearchBar from './components/SearchBar';
import Sentence from './components/Sentence';
import TodoList from './components/TodoList';



function App() {

  
  const [inputText, setInputText] =useState("");
  const [todos, setTodos] = useState([]);
  const [status, setStatus] = useState("all");
  const [filteredTodos, setFilteredTodos] = useState([]);

  // Get Local Todos only when starting the page ( once )
  useEffect(()=> {
    getLocalTodos();
  },
    []
    )
  // EveryTime Todos or Status changes, start filterHandler Function
  useEffect(()=> {
    filterHandler();
    saveLocalTodos();
  },
  [todos, status]
  )

  
  function filterHandler(){
    switch(status){
      case 'completed':
        setFilteredTodos(todos.filter(todo => todo.completed === true));
        break;
      
      case 'uncompleted':
        setFilteredTodos(todos.filter(todo => todo.completed === false));
        break;
      
      default:
      setFilteredTodos(todos);
      break;
    
    }
  }


  function saveLocalTodos(){
      localStorage.setItem('todos', JSON.stringify(todos))
  }

  function getLocalTodos(){
    if(localStorage.getItem('todos') === null){
      localStorage.setItem('todos', JSON.stringify([]))
    }else{ 
      let todoLocal = JSON.parse(localStorage.getItem('todos'))
      setTodos(todoLocal)
    }
  }

  return (
    <div className="App">
      <header>
        <h1>Mel's ToDo List</h1>
      </header>

      <Sentence />

      <SearchBar />      
      <Form 
        todos={todos} 
        setTodos={setTodos}  
        inputText={inputText}
        setInputText={setInputText}
        setStatus={setStatus}
      />

      <TodoList 
        setTodos={setTodos} 
        filteredTodos={filteredTodos} 
        todos={todos} 
      />
    </div>
  );
}

export default App;
