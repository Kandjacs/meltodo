function SearchBar(){


  return(
    <div className="search">
      <form method="get" 
            id="search" 
            action="http://duckduckgo.com/" 
            target="_blank" >

        <input id="searchBar" 
              type="text" 
              name="q" 
              placeholder="Search..."/>

        <input className="searchBtn" 
              type="submit" 
              value="DuckDuckGo Search"/>
      </form>
    </div>
  )
}


export default SearchBar;