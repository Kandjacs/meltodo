

function Form({setInputText, inputText, todos, setTodos, setStatus}){

  const inputTextHandler = (e) =>{
    // console.log(e.target.value);
    setInputText(e.target.value);
  }
  const submitTodoHandler = (e) =>{
    // console.log("heeeeeyee")
    e.preventDefault();
    setTodos([ 
      ...todos, 
      {
        text: inputText, 
        completed: false, 
        id: Math.random()*999  // A changer avec une incrémentation
      } 
    ]);
    setInputText("");
      
  }

  const statusHandler = (e) => {
    setStatus(e.target.value)
  }

  return(
    <div className="form">
      <form className="todoInput">
        <input id="todoForm" value={inputText} onChange={inputTextHandler} type="text" className="todoInput"/>
        <button onClick={submitTodoHandler} type="submit" className="todoButton">
          <i className="fas fa-plus-square"></i>
        </button>
        <div className="select">
          <select onChange={statusHandler} name="todos" className="filterTodo">
            <option value="all">All</option>
            <option value="completed">Completed</option>
            <option value="uncompleted">Uncompleted</option>
          </select>
        </div>
      </form>
    </div>
  );
}

export default Form;

