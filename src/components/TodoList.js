import Todo from "./Todo";



function TodoList({ todos, setTodos, filteredTodos }){


  return(

    <div className="todoContainer">
      <ul className="todoList">
        {filteredTodos.map((todo) =>(
          <Todo 
            setTodos={setTodos}
            todos={todos}
            key={todo.id}
            todo={todo}
            text={todo.text} 
          />
        ))}
      </ul>
    </div>
  )
}

export default TodoList