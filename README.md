Mel's Todo
==========

Author : Kévin Gonzales

Simple Todo App created with React.

## RoadMap

[] Todo
  + [] Add Titles and bodies of Todo
  + [] Add Dates in the Todos ( Initial / DueDate )
  + [] Confirmation to delete Todos ?

[x] SearchBar

[] Motivational Messages
  + [] Change only on refresh or every 10 minutes ?